var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var express = require('express');
var app = express();
var path = require('path');
var fs = require('fs');
var port     = process.env.PORT || 3000;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');
//var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
//var session = require('express-session');

var configDB = require('./config/database.js');

mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
	//app.use(cookieParser()); // read cookies (needed for auth)
	app.use(bodyParser()); // get information from html forms

	app.set('view engine', 'ejs'); // set up ejs for templating

	// required for passport
	//app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
	app.use(passport.initialize());
	app.use(passport.session()); // persistent login sessions
	app.use(flash()); // use connect-flash for flash messages stored in session
	app.use(bodyParser.json());

var url = 'mongodb://localhost:27017/BAARI_TESTI';

var dir = path.join(__dirname, 'public');

var mime = {
    html: 'text/html',
    txt: 'text/plain',
    css: 'text/css',
    gif: 'image/gif',
    jpg: 'image/jpeg',
    png: 'image/png',
    svg: 'image/svg+xml',
    js: 'application/javascript'
};


app.get('/drinks/submit', function(req, res) {
  res.sendFile(path.join(__dirname, 'drink'));
});

app.post('/drinks/submit', function(req, res) {
  // Insert JSON straight into MongoDB
	console.log(req.body.drink);
	MongoClient.connect(url, function(err, db) {
		if(err) { return console.dir(err); }
		//if null ei mitää
		var myJson = JSON.parse(req.body.drink);

		if(myJson.name != null) {
			db.collection('Reseptit').insert(myJson, function (err, result) {
	    	if (err) {
					res.send('Error');
					console.dir(err);
				}
	    	else
	      	res.send('Success');
			});
		}
		else
			res.send('Success');

	db.close;
  });
});

app.get('/drinks/all', function(req, res) {

  var drinks_json = [];

  MongoClient.connect(url, function(err, db) {
    if(err) { return console.dir(err); }
    var collection = db.collection('Reseptit');
    var stream = collection.find({},{_id:0,name:1}).stream();
    stream.on("data", function(drink) {
      drinks_json.push(drink);
    });
    stream.on("end", function() {
      db.close;

      res.json(drinks_json.sort(function(a,b){
        return a.name.localeCompare(b.name);
      }));

			//res.json(drinks_json);
    });
  });
});


app.get('/ingredients', function(req, res) {
  var ingredinets_json = [];

  MongoClient.connect(url, function(err, db) {
    if(err) { return console.dir(err); }
    var collection = db.collection('Reseptit');
    var stream = collection.find({},{_id:0,"ingredients.ingredient":1}).stream();
    stream.on("data", function(ingredient) {
      ingredinets_json.push(ingredient);
    });
    stream.on("end", function() {
      db.close;
      var temp_json = [];
      for(var i = 0; i < ingredinets_json.length; i++) {
        for(var n = 0; n < ingredinets_json[i]['ingredients'].length; n++) {
          temp_json.push(ingredinets_json[i]['ingredients'][n]['ingredient']);
        }
      }
      var return_json = temp_json.filter(function(elem, pos) {
        return temp_json.indexOf(elem) == pos;
      })
      return_json.sort();
      res.json(return_json);
    });
  });
});

app.get('/drinks/search', function(req, res) {

  var drinks_json = [];

  if(req.query.i != null) {
   // var ingredientArray = Object.values(JSON.parse(req.query.i));
    try {
      var ingredientArray = JSON.parse(req.query.i);
        MongoClient.connect(url, function(err, db) {
        if(err) { return console.dir(err); }
        var collection = db.collection('Reseptit');
        if(req.query.s == "inverted")
        {
          var query = {ingredients:{$not:{$elemMatch:{ingredient:{$nin:ingredientArray}}}}};
        }
        else
        {
          var query = {ingredients:{$elemMatch:{ingredient:{$in:ingredientArray}}}};
        }
        //var query = {ingredients:{$not:{$elemMatch:{ingredient:{$nin:["Rommi", "Kola"]}}}}};
        //var query = {ingredients:{$not:{$elemMatch:{ingredient:{$nin:testArray}}}}};
        //console.log("Query: " + query.toString());
        var stream = collection.find(query,{_id:0,name:1}).stream();
        stream.on("data", function(drink) {
          drinks_json.push(drink);
        });
        stream.on("end", function() {
          db.close;

          res.json(drinks_json.sort(function(a,b){
            return a.name.localeCompare(b.name);
          }));

					//res.json(drinks_json);
        });
      });
    }
    catch (e) {
      var myError = '{"error": "Invalid i parameter!"}';
      res.json(JSON.parse(myError));
    }
    //var testArray = ["Rommi", "Kola"];
    //console.log(ingredientArray);
  }

  else if(req.query.d != null) {
    //var ingredientArray = Object.values(JSON.parse(req.param('i')));
    try {
      var name = req.query.d;
      name = name.replace(/[^A-ZÅÄÖa-zåäö0-9' ]/g, '');

      MongoClient.connect(url, function(err, db) {
        if(err) { return console.dir(err); }
        var collection = db.collection('Reseptit');
        var query = {name:name};

        var stream = collection.find(query,{_id:0}).stream();
        stream.on("data", function(drink) {
          drinks_json.push(drink);
        });
        stream.on("end", function() {
          db.close;
					/*
					if (res != null) {*/
						res.json(drinks_json.sort(function(a,b){
	            return a.name.localeCompare(b.name);
	          }));/*
					}
					*/
					//res.json(drinks_json);
        });
      });
    }
    catch (e) {
      var myError = '{"error": "Invalid d parameter!"}';
      res.json(JSON.parse(myError));
    }
  }

  else {
    res.json(drinks_json);
  }
});

require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// launch ======================================================================
app.listen(port);
console.log('Listening at: ' + port);
