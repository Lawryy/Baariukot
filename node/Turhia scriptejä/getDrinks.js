var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var express = require('express')
var app = express()

var url = 'mongodb://localhost:27017/BAARI_TESTI';

var drinks_json = [];

MongoClient.connect(url, function(err, db) {
  if(err) { return console.dir(err); }
  var collection = db.collection('Reseptit');
  var stream = collection.find({},{_id:0,name:1}).stream();
  stream.on("data", function(drink) {
    drinks_json.push(drink);
  });
  stream.on("end", function() {
    db.close;
  });
});

app.get('/drinks/all', function(req, res) {
  res.json(drinks_json);
});

app.listen(3000);
