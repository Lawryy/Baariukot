var express = require('express')

var app = express()

app.get('/footballPlayers', function(req, res) {
	var json_string = {"players":[
            {"name":"Messi", "goals":8},
            {"name":"Ronaldo", "goals":22},
            {"name":"Costa", "goals":20},
            {"name":"Neymar", "goals":13},
            {"name":"Arabi", "goals":6},
            {"name":"Bale", "goals":3},
            {"name":"Toquero", "goals":0}]};
  res.json(json_string)
})

app.listen(3000)
